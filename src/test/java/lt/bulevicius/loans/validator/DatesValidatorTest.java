package lt.bulevicius.loans.validator;

import org.junit.Test;

import java.time.format.DateTimeParseException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DatesValidatorTest {

    @Test
    public void failsWhenDatesAreEmpty() {
        UnsupportedOperationException exception = assertThrows(
                UnsupportedOperationException.class,
                () -> { new DatesValidator().validate("", ""); }
        );

        assertEquals("Dates can't be empty", exception.getMessage());

    }

    @Test()
    public void failsWithWrongDateFormat() {
        DateTimeParseException exception = assertThrows(
                DateTimeParseException.class,
                () -> { new DatesValidator().validate("2021-01-01", "2020-01-01.1234567"); }
        );

        assertEquals("Text '2020-01-01.1234567' could not be parsed, unparsed text found at index 10", exception.getMessage());
    }

    @Test
    public void valitesDates() {
        new DatesValidator().validate("2020-01-01", "2021-01-01") ;
    }

    @Test
    public void failsWhenDateFromIsGreaterThanDateTo() {
        UnsupportedOperationException exception = assertThrows(
                UnsupportedOperationException.class,
                () -> { new DatesValidator().validate("2021-01-01", "2020-01-01"); }
        );

        assertEquals("Incorrect dates range", exception.getMessage());
    }
}
