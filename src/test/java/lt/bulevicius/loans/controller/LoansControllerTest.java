package lt.bulevicius.loans.controller;

import lt.bulevicius.loans.transactions.Transactions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LoansControllerTest {

    private LoansController loansConstroller;

    @Before
    public void init() {
        loansConstroller = new LoansController();
    }

    @Test
    public void failsNegativeDepositAmount() {
        UnsupportedOperationException exception = assertThrows(
                UnsupportedOperationException.class,
                () -> {
                    loansConstroller.deposit(-5);
                }
        );

        assertEquals("Amount can't be negative or 0", exception.getMessage());
    }

    @Test
    public void failsNegativeWithdrawAmount() {
        UnsupportedOperationException exception = assertThrows(
                UnsupportedOperationException.class,
                () -> {
                    loansConstroller.withdraw(-5);
                }
        );

        assertEquals("Amount can't be negative or 0", exception.getMessage());
    }

    @Test
    public void depositesMoney() {
        loansConstroller.deposit(7.0);
        assertEquals(37.0, loansConstroller.balance());
    }

    @Test
    public void withdrawMoney() {
        loansConstroller.withdraw(3.0);
        assertEquals(34.0, loansConstroller.balance());
    }

    @Test
    public void returnsTransactionsSize() {
        List<Transactions> transactions = loansConstroller.statements("2020-01-01", "2021-01-01");
        assertEquals(4, transactions.size());
    }
}
