package lt.bulevicius.loans.controller;

import lt.bulevicius.loans.service.OperationService;
import lt.bulevicius.loans.transactions.Transactions;
import lt.bulevicius.loans.validator.DatesValidator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/")
public class LoansController {

    OperationService operationService = new OperationService();

    @RequestMapping("deposit/{amount}")
    public double deposit(@PathVariable double amount) {
        if (amount <= 0) {
            throw new UnsupportedOperationException("Amount can't be negative or 0");
        }
        operationService.saveTransaction(amount, "deposit");
        return amount;
    }

    @RequestMapping("withdraw/{amount}")
    public double withdraw(@PathVariable double amount) {
        if (amount <= 0) {
            throw new UnsupportedOperationException("Amount can't be negative or 0");
        }
        if (amount > operationService.getAccountBalance()) {
            throw new UnsupportedOperationException("Insufficient account balance");
        }
        operationService.saveTransaction(amount * -1, "withdraw");
        return amount;
    }

    @RequestMapping("balance")
    public double balance() {
        return operationService.getAccountBalance();
    }

    @RequestMapping("statements/{dateFrom}/{dateTo}")
    public List<Transactions> statements(@PathVariable String dateFrom, @PathVariable String dateTo) {
        new DatesValidator().validate(dateFrom, dateTo);
        LocalDate localDateFrom = LocalDate.parse(dateFrom);
        LocalDate localDateTo = LocalDate.parse(dateTo);
        return operationService.queryOfTransactions("SELECT * FROM TRANSACTIONS WHERE EVENT_TIME BETWEEN '" + localDateFrom + "' and '" + localDateTo + " 23:59:59.999" + "'");
    }
}
