package lt.bulevicius.loans.service;

import lt.bulevicius.loans.transactions.Transactions;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OperationService {

    private static final String USER = "sa";
    private static final String PASSWORD = "sa";

    public void saveTransaction(double amount, String transactionType) {
        try {
            initialCall().execute("INSERT INTO TRANSACTIONS (AMOUNT, TYPE, EVENT_TIME) VALUES (" + amount + ", '" + transactionType + "', '" + LocalDateTime.now() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double getAccountBalance() {
        return queryResponse("SELECT SUM(AMOUNT) FROM TRANSACTIONS");
    }

    private double queryResponse(String query) {
        double sum = 0;
        try {
            sum = getBalance(initialCall().executeQuery(query));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sum;
    }

    public List<Transactions> queryOfTransactions(String query) {
        List<Transactions> transactions = new ArrayList<>();
        try {
            transactions = transactionsList(initialCall().executeQuery(query));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactions;
    }

    private List<Transactions> transactionsList(ResultSet res) throws SQLException {
        List<Transactions> transactions = new ArrayList<>();
        while (res.next()) {
            Transactions tr = new Transactions(
                    res.getInt("TRANSACTIONID"),
                    res.getDouble("AMOUNT"),
                    res.getString("TYPE"),
                    res.getString("EVENT_TIME"));
            transactions.add(tr);
        }
        return transactions;
    }

    private double getBalance(ResultSet res) throws SQLException {
        double sum = 0;
        while (res.next()) {
            sum = res.getInt(1);
        }
        return sum;
    }

    private Statement initialCall() {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.h2.Driver");
            c = DriverManager.getConnection("jdbc:h2:mem:database", USER, PASSWORD);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            c.setAutoCommit(true);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return stmt;
    }
}
