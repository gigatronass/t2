package lt.bulevicius.loans.transactions;

public class Transactions {

    private int transactionId;
    private double amount;
    private String type;
    private String time;

    public Transactions(int transactionid, double amount, String type, String time) {
        this.transactionId = transactionid;
        this.amount = amount;
        this.type = type;
        this.time = time;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}