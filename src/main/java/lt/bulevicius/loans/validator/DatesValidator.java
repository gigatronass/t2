package lt.bulevicius.loans.validator;

import org.springframework.expression.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class DatesValidator {

    final static String DATE_FORMAT = "yyyy-MM-dd";

    public DatesValidator() {
    }

    public void validate(String dateFrom, String dateTo) {
        checkIsEmpty(dateFrom, dateTo);
        try {
            isDateValid(dateFrom);
            isDateValid(dateTo);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        checkIsPeriodValid(dateFrom, dateTo);
    }

    private void checkIsPeriodValid(String dateFrom, String dateTo) {
        LocalDate localDateFrom = LocalDate.parse(dateFrom);
        LocalDate localDateTo = LocalDate.parse(dateTo);
        if (localDateFrom.isAfter(localDateTo)) {
            throw new UnsupportedOperationException("Incorrect dates range");
        }
    }

    private void checkIsEmpty(String dateFrom, String dateTo) {
        if (dateFrom.isEmpty() || dateTo.isEmpty()) {
            throw new UnsupportedOperationException("Dates can't be empty");
        }
    }

    private static boolean isDateValid(String date) throws java.text.ParseException {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
